package br.edu.usj.ads.lpii;

import javax.faces.bean.ManagedBean;
import javax.faces.bean.RequestScoped;

@ManagedBean
@RequestScoped
public class BeanCalculadora {

    private int a;
    private int b;
    private int resultado;
    
    public void setA (int a) { this.a = a; }
    public void setB (int b) { this.b = b; }
    public void setResultado (int resultado) { this.resultado = resultado; }
    
    public int getA() { return a; }
    public int getB() { return b; }
    public int getResultado() { return resultado; }
    
    public String somar() {
        resultado = a + b;
        return "resultado";
    }        
}
